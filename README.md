# A CV or résumé using LaTeX with YAML through Pandoc making use of Gitlab continuous integration.

**This projects generates cv's and now also cover letters (in the same fashion)!**

> LaTeX resumes can be considered to be a secret handshake of sorts, something that makes me significantly more likely to be inclined to hire a candidate.  
> &mdash;<cite>[zackelan](https://news.ycombinator.com/item?id=10452606)<cite> on HN

**A full article about this project is on [Medium](https://medium.com/@dimitrieh/a-curriculum-vitae-latex-typesetting-automation-adventure-with-gitlab-6ac233c0b66b#.8ca9vxgwf)**

**Interested in more? See: [Terms of Service CI Boilerplate](https://gitlab.com/dimitrieh/terms-of-service-CI-boilerplate)**

Résumé:

![preview-cv](https://gitlab.com/dimitrieh/curriculumvitae-ci-boilerplate/raw/master/preview.jpg)

The pdf's are reachable by url via gitlab pages, see the following examples:  [CVpdfA](http://dimitrieh.gitlab.io/curriculumvitae-ci-boilerplate/FriedrichNietzscheCV_A.pdf) and [CVpdfB](http://dimitrieh.gitlab.io/curriculumvitae-ci-boilerplate/FriedrichNietzscheCV_B.pdf)

Cover Letter:

![preview-cover-letter](https://gitlab.com/dimitrieh/curriculumvitae-ci-boilerplate/raw/master/previewcoverletter.jpg)

The pdf's are reachable by url via gitlab pages, see the following examples:  [CoverLetterpdfA](http://dimitrieh.gitlab.io/curriculumvitae-ci-boilerplate/FriedrichNietzscheCoverLetter_A.pdf), [CoverLetterpdfB](http://dimitrieh.gitlab.io/curriculumvitae-ci-boilerplate/FriedrichNietzscheCoverLetter_B.pdf) and [CoverLetterpdfC](http://dimitrieh.gitlab.io/curriculumvitae-ci-boilerplate/FriedrichNietzscheCoverLetter_C.pdf)

**Run it with**
```
docker run -it -v `pwd`:/source strages/pandoc-docker /bin/bash
```

*Note that after the container is running you have to run the following:*

```
cp fonts/* /usr/share/fonts/
fc-cache -f -v
make
```

**To use the `dropbox_uploader` script**

There are two ways to authorize the `dropbox_uploader` script to allow the upload of your Résumé and Cover Letters: Manual and Automatic

***Automatic***

Remove the file `dropbox_uploader_config` and run the command:

```
./dropbox_uploader -f dropbox_uploader_config
```

And follow the steps oulined by the script.  The same steps are shown bellow.

***Manual***

Pretty much the same steps will be taken on both ways, but here you will edit the configuration file yourself.

```
 1) Open the following URL in your Browser, and log in using your account: <a href="https://www.dropbox.com/developers/apps">https://www.dropbox.com/developers/apps</a>
 2) Click on "<strong>Create App</strong>", then select "<strong>Dropbox API app</strong>"
 3) Now go on with the configuration, choosing the app permissions and access restrictions to your DropBox folder
 4) Enter the "<strong>App Name</strong>" that you prefer (e.g. MyUploader258582771510240)

 Now, click on the "<strong>Create App</strong>" button.

 When your new App is successfully created, please click on the Generate button
 under the 'Generated access token' section, then copy the new access token.
 ```

Edit the file `dropbox_uploader_config`, and paste the token:

```
 OAUTH_ACCESS_TOKEN=<strong>&lt;ACCESS_TOKEN_HERE&gt;</strong>
```

Pull requests and issues welcome. Fork away :)

## Contributing

Please mention me (@dimitrieh) in any issues or merge request with your changes :wink:

## Recommended readings

- [Practical Typography](http://practicaltypography.com/)
- [ShareLatex CV or Resume Templates](https://www.sharelatex.com/templates/cv-or-resume)
- [Overleaf CV or Resume Templates](https://www.overleaf.com/gallery/tagged/cv)
- [Please Hack my Resume](http://please.hackmyresume.com/)
- [ASK DN: What are the optimal layout and desired curriculum vitae characteristics (design/digital) companies look for?](https://www.designernews.co/stories/69097)
- [Ask HN: What are the optimal layout and Desired CV Characteristics?](https://news.ycombinator.com/item?id=11700086)

## Credits

- Based upon Mattia Tezzele's [tex-boilerplates](http://mrzool.cc/tex-boilerplates/) | [github](https://github.com/mrzool/cv-boilerplate) which is based on Dario Taraborelli's [cvtex](https://github.com/dartar/cvtex)

## License

- License: [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
- Linux Libertine O Font License: [GPL](http://www.gnu.org/licenses/gpl.html) and [OFL](http://scripts.sil.org/OFL)

hello world
